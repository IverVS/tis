import { Component, OnInit } from '@angular/core';
import {EstudianteService} from "../service/estudiante/estudiante.service";
import { FormGroup, FormBuilder, Validator, Validators, EmailValidator } from '@angular/forms';
import { Estudiante} from '../models/estudiante/estudiante';
import { Router, Route } from '@angular/router';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-registroestudiante',
  templateUrl: './registroestudiante.component.html',
  styleUrls: ['./registroestudiante.component.css']
})
export class RegistroestudianteComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router, 
    private estudianteService: EstudianteService,
    )

  {
    this.formulario = this.formBuilder.group({
    nombre: ['',Validators.required],
    apPaterno: ['',Validators.required],
    apMaterno: ['',Validators.required],
    cedula: ['',Validators.required],
    celular: ['',Validators.required],
    codSis: ['',Validators.required],
    contrasenia: ['',Validators.required],
    email: ['',Validators.email]
    })
  }

  ngOnInit() 
  { 
  }
  get f() { return this.formulario.controls; }
  
  crearEstudiante()
  {
    this.submitted = true;

    if (this.formulario.invalid) 
    {
      return;
    }
      const Swal = require('sweetalert2')
      Swal.fire({
      title: 'Estas Seguro de Guardar?',
      text: "porque se guardaran estos datos",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      
      cancelButtonText: 'Cancelar'
      }).then((result) => 
      {
        if (result.value) 
        {
          console.log("formulario funcionando", this.formulario.value) ;
          this.estudianteService.guardar(this.formulario.value).subscribe
          (
            (estudiante: Estudiante) => 
            {
                if(estudiante == null)
                {
                  
                alert("Estudiante ya existe");
                 window.location.reload();
                }
                else
                {
                  console.log(estudiante);
                  localStorage.setItem("estudianteRegistrado", JSON.stringify(estudiante));
                  Swal.fire(
                    'Bienvenido!  ',
                    'Ahora puedes iniciar tu Sesión',
                    'success')
                    this.router.navigate(['homeestudiante']);
                }
            },
            (error) => 
            {
              console.log("Error!!!", error);
            }
          )
        }
      })
    }
  
}
