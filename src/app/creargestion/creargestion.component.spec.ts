import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreargestionComponent } from './creargestion.component';

describe('CreargestionComponent', () => {
  let component: CreargestionComponent;
  let fixture: ComponentFixture<CreargestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreargestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreargestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
