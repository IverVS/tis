import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GestionService } from '../service/gestion/gestion.service';
import { Gestion } from '../models/gestion/gestion';

@Component({
  selector: 'app-creargestion',
  templateUrl: './creargestion.component.html',
  styleUrls: ['./creargestion.component.css']
})
export class CreargestionComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;
  gestiones: Gestion[];

  constructor(private router: Router, private formBuilder: FormBuilder, private service: GestionService)
  { 
    this.formulario = this.formBuilder.group(
      {
        nombre:["",Validators.required]
      })
  }

  ngOnInit() 
  {
    this.service.listar().subscribe(data => (this.gestiones = data));
  }
  get f() { return this.formulario.controls}

  registrarGestion()
  {
    this .submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de Guardar la Gestión?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log(this.formulario.value);
        this.service.registrar(this.formulario.value).subscribe
        (
          (gestion: Gestion) =>
          {
            console.log(gestion);
          },
          (error) =>
          {
            console.log("Error...!!!", error);
          }
        )
        Swal.fire(
          'Registro de la Gestión exitosa!',
          'Muy Bien',
          'success'
        )
        this.router.navigate(['/inicioadministrador'])
      }
    })
  }

}
