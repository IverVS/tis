import { Estudiante } from '../estudiante/estudiante';

export class Asistencia 
{
    idAsistencia: number;
    estudiante: Estudiante;
    fecha: string;
}
