export class Estudiante 
{
    idEstudiante:number
    nombre: string
    apPaterno: string
    apMaterno: string
    cedula: string
    celular: string
    codSis: string
    contrasenia: string
    email: string
  }
