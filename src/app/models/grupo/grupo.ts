import { Docente } from '../docente/docente';
import { Gestion } from '../gestion/gestion';
import { Materia } from '../materia/materia';

export class Grupo 
{
    idGrupo: number;
    docente: Docente;
    materia: Materia;
    gestion: Gestion;
}