import { Grupo } from 'src/app/models/grupo/grupo';
import { Laboratorio } from 'src/app/models/laboratorio/laboratorio';
import { Horario } from 'src/app/models/horario/horario';
import { Tutor } from './../tutor/tutor';
export class Curso {

    idCurso: number;
    laboratorio: Laboratorio;
    tutor: Tutor;
    horario: Horario;
    dia: string;
    grupo: Grupo;
}
