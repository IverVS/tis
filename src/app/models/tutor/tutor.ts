export class Tutor 
{
    id?: number
    nombre: string
    apMaterno: string
    apPaterno: string
    cedula: string
    celular: string
    codSis: string
    contrasenia: string
    email: string
  }
