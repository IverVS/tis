import { Estudiante } from '../estudiante/estudiante';
import { Curso } from '../curso/curso';

export class Inscripcion 
{
    idInscripcion: number;
    estudiante: Estudiante;
    curso: Curso;
}
