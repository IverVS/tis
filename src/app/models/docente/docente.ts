export class Docente 
{
    idDocente: number
    nombre: string
    apMaterno: string
    apPaterno: string
    celular: string
    cedula: string
    codSis: string
    contrasenia: string
    email: string
  }
