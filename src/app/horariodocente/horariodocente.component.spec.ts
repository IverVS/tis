import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorariodocenteComponent } from './horariodocente.component';

describe('HorariodocenteComponent', () => {
  let component: HorariodocenteComponent;
  let fixture: ComponentFixture<HorariodocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorariodocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorariodocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
