import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DocenteService } from "../service/docente/docente.service";
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-logindocente',
  templateUrl: './logindocente.component.html',
  styleUrls: ['./logindocente.component.css']
})
export class LogindocenteComponent implements OnInit 
{
  formulariologin: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private docenteService: DocenteService) 
  {
    this.formulariologin = this.formBuilder.group({
      codSis: ['',Validators.required],
      contrasenia: ['',Validators.required],
    }
    )
  }

  ngOnInit() 
  {
  }
  get f() { return this.formulariologin.controls; }

  onSubmit()
  {
    this.submitted = true;

   if (this.formulariologin.valid) {
    const sis = this.formulariologin.get('codSis').value;
    const contra = this.formulariologin.get('contrasenia').value;

    console.log(this.formulariologin.get('contrasenia').value);
    console.log(this.formulariologin.get('codSis').value);

    this.docenteService.validarDoc(sis, contra).subscribe(
      respuesta => {
        if (this.formulariologin.invalid) {
          return;
        }
        localStorage.setItem("docenteLogeado", JSON.stringify(respuesta));
        const Swal = require('sweetalert2')
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Conectado Correctamente'
        })
        this.router.navigate(['iniciodocente']);
      },
      (error) => {
        if (error.status === 404) {
          console.log('no encontrado');
          const Swal = require('sweetalert2')
          Swal.fire({
            type: 'error',
            title: 'Upss...',
            text: '¡Algo salió mal! Verifica que tus datos que sean correctos ',
          })
        } else {
          console.log('error inesperado');
        }
      }
    );
  }
  }

}
