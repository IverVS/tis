import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogindocenteComponent } from './logindocente.component';

describe('LogindocenteComponent', () => {
  let component: LogindocenteComponent;
  let fixture: ComponentFixture<LogindocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogindocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogindocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
