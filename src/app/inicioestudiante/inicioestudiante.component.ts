import { Component, OnInit } from '@angular/core';
import { Estudiante } from '../models/estudiante/estudiante';
import { Router } from '@angular/router';
import { InscripcionService } from '../service/inscripcion/inscripcion.service';
import { Inscripcion } from '../models/inscripcion/inscripcion';

@Component({
  selector: 'app-inicioestudiante',
  templateUrl: './inicioestudiante.component.html',
  styleUrls: ['./inicioestudiante.component.css']
})
export class InicioestudianteComponent implements OnInit 
{
  inscripciones: Inscripcion[];
  estudiante: Estudiante= JSON.parse(localStorage.getItem("estudianteLogeado"));
  constructor(private router: Router,private inscripcionService: InscripcionService) { }

  hideUpdate:boolean = true;
  hidenUpdate:boolean = true;

  infoInscripcion: Inscripcion;

  ngOnInit()
  {
      
   // this.inscripcionService.listar().subscribe(data => (this.inscripciones = data));
    this.inscripcionService.mostrarInscripcionEstudiante(this.estudiante.idEstudiante).subscribe(data=>{
         this.infoInscripcion= data;
         console.log(this.infoInscripcion);
    });
  }
  editEmpleado(): void
  {
    this.hideUpdate = false;
  }
  inscripcion(): void
  {
    this.hidenUpdate = false;
  }
  cerrarSesion(){
    this.router.navigate(['inicio']);
  }

}
