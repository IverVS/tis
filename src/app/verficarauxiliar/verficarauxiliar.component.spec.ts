import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerficarauxiliarComponent } from './verficarauxiliar.component';

describe('VerficarauxiliarComponent', () => {
  let component: VerficarauxiliarComponent;
  let fixture: ComponentFixture<VerficarauxiliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerficarauxiliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerficarauxiliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
