import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verficarauxiliar',
  templateUrl: './verficarauxiliar.component.html',
  styleUrls: ['./verficarauxiliar.component.css']
})
export class VerficarauxiliarComponent implements OnInit 
{
  constructor(private router: Router) { }

  ngOnInit()
  {
  }
  onSubmit(form: NgForm)
  {
    console.log(form.value);
    const Swal = require('sweetalert2')
    if(form.value.password == 'asd')
    {
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Bienvenido a registro',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['registroauxiliar']);
    }
    else
    {
      Swal.fire({
        type: 'error',
        title: 'Ups...',
        text: 'Error de Contraseña!',
      })
    }
  }
}
