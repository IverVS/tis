import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearnuevaactividadComponent } from './crearnuevaactividad.component';

describe('CrearnuevaactividadComponent', () => {
  let component: CrearnuevaactividadComponent;
  let fixture: ComponentFixture<CrearnuevaactividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearnuevaactividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearnuevaactividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
