import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crearnuevaactividad',
  templateUrl: './crearnuevaactividad.component.html',
  styleUrls: ['./crearnuevaactividad.component.css']
})
export class CrearnuevaactividadComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  actividad()
  {
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de Guardar la Actividad?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        Swal.fire(
        'Registro de Actividad exitoso!',
        'Muy Bien',
        'success'
        )
        this.router.navigate(['crearactividad']);
      }
    })
  }
}
