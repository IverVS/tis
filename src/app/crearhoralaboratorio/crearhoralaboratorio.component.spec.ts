import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearhoralaboratorioComponent } from './crearhoralaboratorio.component';

describe('CrearhoralaboratorioComponent', () => {
  let component: CrearhoralaboratorioComponent;
  let fixture: ComponentFixture<CrearhoralaboratorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearhoralaboratorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearhoralaboratorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
