import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LaboratorioService } from '../service/laboratorio/laboratorio.service';
import { Laboratorio } from '../models/laboratorio/laboratorio';

@Component({
  selector: 'app-crearhoralaboratorio',
  templateUrl: './crearhoralaboratorio.component.html',
  styleUrls: ['./crearhoralaboratorio.component.css']
})
export class CrearhoralaboratorioComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;

  constructor(private router: Router, private formBuilder: FormBuilder, private service: LaboratorioService) 
  { 
    this.formulario = this.formBuilder.group(
    {
      nombre:["",Validators.required]
    })
  }

  ngOnInit() 
  {
  }
  get f() { return this.formulario.controls}

  registrarLaboratorio()
  {
    this .submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de Guardar el Laboratorio?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log(this.formulario.value);
        this.service.registrar(this.formulario.value).subscribe
        (
          (laboratorio: Laboratorio) =>
          {
            console.log(laboratorio);
            localStorage.setItem("laboratorioRegistrdo", JSON.stringify(laboratorio));

          },
          (error) =>
          {
            console.log("Error...!!!", error);
          }
        )
        Swal.fire(
          'Registro de Laboratorio exitoso!',
          'Muy Bien',
          'success'
        )
        this.router.navigate(['/inicioadministrador'])
      }
    })
  }

}
