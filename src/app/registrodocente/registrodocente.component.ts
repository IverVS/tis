import { Component, OnInit } from '@angular/core';
import {DocenteService} from "../service/docente/docente.service";
import { FormGroup, FormBuilder, Validator, Validators, EmailValidator } from '@angular/forms';
import { Docente} from '../models/docente/docente';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registrodocente',
  templateUrl: './registrodocente.component.html',
  styleUrls: ['./registrodocente.component.css']
})
export class RegistrodocenteComponent implements OnInit 
{
  formularioDocente: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router, 
    private docenteService: DocenteService)

  {
    this.formularioDocente = this.formBuilder.group({
    nombre: ['',Validators.required],
    apPaterno: ['',Validators.required],
    celular: ['',Validators.required],
    apMaterno: ['',Validators.required],
    cedula: ['',Validators.required],
    codSis:['',Validators.required],
    contrasenia:['',Validators.required],
    email: ['',Validators.email]
    })
  }

  ngOnInit() 
  { 
  }
  get f() { return this.formularioDocente.controls; }
  
  crearDocente()
  {
    this.submitted = true;

    if (this.formularioDocente.invalid) 
    {
      return;
    }
      const Swal = require('sweetalert2')
      Swal.fire({
      title: 'Estas Seguro de Guardar?',
      text: "porque se guardaran estos datos",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      
      cancelButtonText: 'Cancelar'
      }).then((result) => 
      {
        if (result.value) 
        {
          console.log("formulario funcionando", this.formularioDocente.value) ;
          this.docenteService.guardarDocente(this.formularioDocente.value).subscribe
          (
            (docente: Docente) => 
            {
                if(docente == null){
                alert("Estudiante ya existe");
                 window.location.reload();
                }
                else{
                  console.log(docente);
                  localStorage.setItem("docenteRegistrado", JSON.stringify(docente));
                  Swal.fire(
                    'Bienvenido!  ',
                    'Ahora puedes iniciar tu Sesión',
                    'success')
                    this.router.navigate(['homedocente']);
                }
            },
            (error) => 
            {
              console.log("Error!!!", error);
            }
          )
        }
      })
  }
}
