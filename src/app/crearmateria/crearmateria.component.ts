import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MateriaService } from '../service/materia/materia.service';
import { Materia } from '../models/materia/materia';

@Component({
  selector: 'app-crearmateria',
  templateUrl: './crearmateria.component.html',
  styleUrls: ['./crearmateria.component.css']
})
export class CrearmateriaComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;

  constructor(private router: Router, private formBuilder: FormBuilder, private service: MateriaService) 
  { 
    this.formulario = this.formBuilder.group({
      nombre: ["",Validators.required]
    })
  }

  ngOnInit() {
  }
  get f() { return this.formulario.controls}
  
  registrarMateria()
  {
    this.submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de Guardar la Materia?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log(this.formulario.value);
        this.service.registrar(this.formulario.value).subscribe
        (
          (materia: Materia) =>
          {
            console.log(materia);
            Swal.fire(
              'Registro de Materia exitoso!',
              'Muy Bien',
              'success'
            )
          },
          (error) =>
          {
            console.log("Error...!!!", error);
          }
        )

        this.router.navigate(['/inicioadministrador'])
      }
    })
  }

}
