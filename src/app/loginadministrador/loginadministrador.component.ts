import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginadministrador',
  templateUrl: './loginadministrador.component.html',
  styleUrls: ['./loginadministrador.component.css']
})
export class LoginadministradorComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm)
  {
    console.log(form.value);
    const Swal = require('sweetalert2')
    if(form.value.password == 'asd')
    {
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Bienvenido a inicio de administrador',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['inicioadministrador']);
    }
    else
    {
      Swal.fire({
        type: 'error',
        title: 'Ups...',
        text: 'Error de Contraseña!',
      })
    }
  }
}
