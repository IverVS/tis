import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HorarioService } from '../service/horario/horario.service';
import { Horario } from '../models/horario/horario';

@Component({
  selector: 'app-crearhorario',
  templateUrl: './crearhorario.component.html',
  styleUrls: ['./crearhorario.component.css']
})
export class CrearhorarioComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;

  constructor(private router: Router, private formBuilder: FormBuilder, private service: HorarioService) 
  { 
    this.formulario = this.formBuilder.group(
      {
        inicio: ["", Validators.required],
        fin: ["", Validators.required]
      }
    )
  }

  ngOnInit() {
  }

  get f() { return this.formulario.controls}

  registrarHorario()
  {
    this .submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de guardar ese horario?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log(this.formulario.value);
        this.service.registrar(this.formulario.value).subscribe
        (
          (horario: Horario) =>
          {
            console.log(horario);
          },
          (error) =>
          {
            console.log("Error...!!!", error);
          }
        )
        Swal.fire(
          'Registro de Horario exitoso!',
          'Muy Bien',
          'success'
        )
        this.router.navigate(['/inicioadministrador'])
      }
    })
  }
}
