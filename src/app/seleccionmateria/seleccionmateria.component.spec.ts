import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionmateriaComponent } from './seleccionmateria.component';

describe('SeleccionmateriaComponent', () => {
  let component: SeleccionmateriaComponent;
  let fixture: ComponentFixture<SeleccionmateriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionmateriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionmateriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
