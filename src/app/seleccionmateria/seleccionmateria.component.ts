import { Component, OnInit } from '@angular/core';
import { Materia } from '../models/materia/materia';
import { Router } from '@angular/router';
import { MateriaService } from '../service/materia/materia.service';

@Component({
  selector: 'app-seleccionmateria',
  templateUrl: './seleccionmateria.component.html',
  styleUrls: ['./seleccionmateria.component.css']
})
export class SeleccionmateriaComponent implements OnInit 
{
  materias: Materia[];
  constructor(private router: Router, private service: MateriaService) 
  {
  }

  ngOnInit() 
  {
    //this.service.getCustomers().subscribe(data => (this.customers = data));
    this.service.listar().subscribe(data => (this.materias =data));
  }

}
