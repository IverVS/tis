import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginauxiliarComponent } from './loginauxiliar.component';

describe('LoginauxiliarComponent', () => {
  let component: LoginauxiliarComponent;
  let fixture: ComponentFixture<LoginauxiliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginauxiliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginauxiliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
