import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TutorService } from "../service/tutor/tutor.service";
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-loginauxiliar',
  templateUrl: './loginauxiliar.component.html',
  styleUrls: ['./loginauxiliar.component.css']
})
export class LoginauxiliarComponent implements OnInit 
{
  formulariologin: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private tutorService: TutorService) 
  {
    this.formulariologin = this.formBuilder.group({
      codSis: ['',Validators.required],
      contrasenia: ['',Validators.required],
    }
    )
  }

  ngOnInit() 
  {
  }
  get f() { return this.formulariologin.controls; }

  onSubmit()
  {
    this.submitted = true;

    if (this.formulariologin.valid) 
    {
      const sis = this.formulariologin.get('codSis').value;
      const contrasenia = this.formulariologin.get('contrasenia').value;

      console.log(sis + contrasenia);

      this.tutorService.validarTutor(sis, contrasenia).subscribe(
        respuesta => {
          if (this.formulariologin.invalid) {
            return;
          }
          localStorage.setItem("tutorLogeado", JSON.stringify(respuesta));
          const Swal = require('sweetalert2')
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'success',
            title: 'Conectado Correctamente'
          })
          this.router.navigate(['inicioauxiliar']);
        },
        (error) => {
          if (error.status === 404) {
            console.log('no encontrado');
            const Swal = require('sweetalert2')
            Swal.fire({
              type: 'error',
              title: 'Upss...',
              text: '¡Algo salió mal! Verifica que tus datos que sean correctos ',
            })
          } else {
            console.log('error inesperado');
          }
        }
      );
    }
  }

}
