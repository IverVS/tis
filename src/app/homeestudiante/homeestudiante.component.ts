import { Component, OnInit, Input } from '@angular/core';
import { Estudiante } from '../models/estudiante/estudiante';
import { EstudianteService } from '../service/estudiante/estudiante.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homeestudiante',
  templateUrl: './homeestudiante.component.html',
  styleUrls: ['./homeestudiante.component.css']
})
export class HomeestudianteComponent implements OnInit 
{
     estudiante: Estudiante= JSON.parse(localStorage.getItem("estudianteRegistrado"));

     //estudiante:Estudiante;

  constructor(private router: Router, private service: EstudianteService) 
  {
      //this.router.navigate(['/']);
    console.log(this.estudiante.nombre);
  }

  ngOnInit() {
      //this.router.navigate(['homeestudiante']);
  }


}
