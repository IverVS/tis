import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeestudianteComponent } from './homeestudiante.component';

describe('HomeestudianteComponent', () => {
  let component: HomeestudianteComponent;
  let fixture: ComponentFixture<HomeestudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeestudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeestudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
