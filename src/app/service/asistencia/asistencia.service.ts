import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Asistencia } from 'src/app/models/asistencia/asistencia';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsistenciaService 
{
  private url="http://localhost:8090/asistencias";
  constructor(private httClient: HttpClient)
  {
  }

  registrar(asistencia: Asistencia): Observable<Asistencia>
  {
    return this.httClient.post<Asistencia>(this.url,asistencia);
  }
}
