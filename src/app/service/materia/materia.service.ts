import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Materia } from 'src/app/models/materia/materia';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MateriaService 
{
  private url='http://localhost:8090/materias';
  constructor(private httpClient: HttpClient) 
  { 
  }
  registrar(materia: Materia): Observable<Materia>
  {
    return this.httpClient.post<Materia>(this.url,materia)
  }
 
  listar(): Observable<Materia[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Materia[])
    );
  }
}
