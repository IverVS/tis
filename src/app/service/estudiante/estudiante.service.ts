import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Estudiante } from '../../models/estudiante/estudiante';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService 
{
  private baseUrl='http://localhost:8090/estudiantes';
  constructor(private httpClient: HttpClient)
  {}

  getEstudianteService(id : number): Observable<Estudiante> {
    return this.httpClient.get(this.baseUrl + '/' + id).pipe(
      map(data => data as Estudiante)
    );
  }

  guardar(estudiante: Estudiante): Observable<Estudiante>
  {
    return this.httpClient.post<Estudiante>(
      'http://localhost:8090/estudiantes',
      estudiante
    )
  }

  validar(codSis:string, contrasenia: string): Observable<Estudiante>
  {
    const estudiante = {
      codSis: codSis,
      contrasenia: contrasenia,
    };

    return this.httpClient.post<Estudiante>(
      `http://localhost:8090/estudiantes/autenticacion`,
      estudiante
    )
  }
  listar(): Observable<Estudiante[]> 
  {
    return this.httpClient.get('http://localhost:8090/estudiantes').pipe(
      map(data => data as Estudiante[])
    );
  }

  buscarCedula(cedula: string): Observable<Estudiante>
  {
    return this.httpClient.get('http://localhost:8090/estudiantes/buscarCedula/'+cedula).pipe(
      map(data => data as Estudiante)
      );
  }
  estudianteId(id: number)
  {
    return this.httpClient.get<Estudiante>(this.baseUrl+"/"+id);
  }
}