import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Gestion } from 'src/app/models/gestion/gestion';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GestionService 
{
  private url='http://localhost:8090/gestiones';
  constructor(private httpClient: HttpClient) 
  { 
  }
  registrar(gestion: Gestion): Observable<Gestion>
  {
    return this.httpClient.post<Gestion>(this.url,gestion)
  }
 
  listar(): Observable<Gestion[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Gestion[])
    );
  }
}
