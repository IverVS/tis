import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Laboratorio } from 'src/app/models/laboratorio/laboratorio';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LaboratorioService 
{
  private url='http://localhost:8090/laboratorios';
  constructor(private httpClient: HttpClient) 
  { 
  }

  registrar(laboratorio: Laboratorio): Observable<Laboratorio>
  {
    return this.httpClient.post<Laboratorio>(this.url,laboratorio)
  }

  listar(): Observable<Laboratorio[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Laboratorio[])
    );
  }
}
