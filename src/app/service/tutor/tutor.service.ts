import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Tutor } from '../../models/tutor/tutor';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TutorService {

  constructor(private httpClient: HttpClient) 
  { }
  
  guardarTutor(tutor: Tutor): Observable<Tutor>
  {
    return this.httpClient.post<Tutor>(
      'http://localhost:8090/tutores',
      tutor
    )
  }
  validarTutor(codSis:string, contrasenia: string): Observable<Tutor>
  {
    const tutor = {
      codSis: codSis,
      contrasenia: contrasenia,
    };

    return this.httpClient.post<Tutor>(
      `http://localhost:8090/tutores/autenticacion`,
      tutor
    )
  }

  listar(): Observable<Tutor[]> 
  {
    return this.httpClient.get('http://localhost:8090/tutores').pipe(
      map(data => data as Tutor[])
    );
  }
}
