import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Docente} from '../../models/docente/docente';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

  constructor(private httpClient: HttpClient) { }
  guardarDocente(docente: Docente): Observable<Docente>
  {
    return this.httpClient.post<Docente>(
      'http://localhost:8090/docentes',
      docente
    )
  }
  validarDoc(codSis:string, contrasenia: string): Observable<Docente>
  {
    const docente = {
      codSis: codSis,
      contrasenia: contrasenia,
    };

    return this.httpClient.post<Docente>(
      `http://localhost:8090/docentes/autenticacion`,
      docente
    )
  }

  listar(): Observable<Docente[]> 
  {
    return this.httpClient.get('http://localhost:8090/docentes').pipe(
      map(data => data as Docente[])
    );
  }
}
