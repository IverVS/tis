import { Observable } from 'rxjs';
import { Curso } from './../../models/curso/curso';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CursoService {

  private url= 'http://localhost:8090/cursos';
  constructor(private httpClient: HttpClient) { }

  registrar(grupo: Curso): Observable<Curso>
  {
    return this.httpClient.post<Curso>(this.url,grupo)
  }

  listar(): Observable<Curso[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Curso[])
    );
  }

  buscarId(id: number)
  {
     return this.httpClient.get<Curso>(`${this.url}/${id}`);
  }
}
