import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Horario } from 'src/app/models/horario/horario';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HorarioService {
 
  private url='http://localhost:8090/horarios';
  constructor(private httpClient : HttpClient) 
  { 
  }

  registrar(horario: Horario): Observable<Horario>
  {
    return this.httpClient.post<Horario>(this.url,horario)
  }

  listar(): Observable<Horario[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Horario[])
    );
  }
}
