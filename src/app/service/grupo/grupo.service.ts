import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Grupo } from 'src/app/models/grupo/grupo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GrupoService 
{
  private url= 'http://localhost:8090/grupos';
  constructor(private httpClient: HttpClient) 
  { 
  }
  registrar(grupo: Grupo): Observable<Grupo>
  {
    return this.httpClient.post<Grupo>(this.url,grupo)
  }

  listar(): Observable<Grupo[]> 
  {
    return this.httpClient.get(this.url).pipe(
      map(data => data as Grupo[])
    );
  }
}
