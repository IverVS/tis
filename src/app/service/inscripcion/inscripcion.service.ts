import { Injectable } from '@angular/core';
import { Inscripcion } from 'src/app/models/inscripcion/inscripcion';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InscripcionService 
{
  private url = 'http://localhost:8090/inscripciones';
  constructor(private httpClient: HttpClient)
  {
  }
  registrar(inscripcion: Inscripcion)
  {
    return this.httpClient.post<Inscripcion>(this.url,inscripcion);
  }
  listar(): Observable<Inscripcion[]> 
  {
    //return this.httpClient.get(this.url);
    return this.httpClient.get(this.url).pipe(
      map(data => data as Inscripcion[])
    );
  }

  mostrarInscripcionEstudiante(id: number)
  {
     return this.httpClient.get<Inscripcion>(`${this.url}/${id}`);
  }
}
