import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerficardocenteComponent } from './verficardocente.component';

describe('VerficardocenteComponent', () => {
  let component: VerficardocenteComponent;
  let fixture: ComponentFixture<VerficardocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerficardocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerficardocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
