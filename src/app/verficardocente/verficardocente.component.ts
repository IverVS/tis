import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-verficardocente',
  templateUrl: './verficardocente.component.html',
  styleUrls: ['./verficardocente.component.css']
})
export class VerficardocenteComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() 
  {
  }

  onSubmit(form: NgForm)
  {
    console.log(form.value);
    const Swal = require('sweetalert2')
    if(form.value.password == 'asd')
    {
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Bienvenido a registro',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['registrodocente']);
    }
    else
    {
      Swal.fire({
        type: 'error',
        title: 'Ups...',
        text: 'Error de Contraseña!',
      })
    }
  }
}
