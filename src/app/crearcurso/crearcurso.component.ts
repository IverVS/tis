import { Curso } from './../models/curso/curso';
import { CursoService } from './../service/curso/curso.service';
import { Grupo } from './../models/grupo/grupo';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LaboratorioService } from '../service/laboratorio/laboratorio.service';
import { HorarioService } from '../service/horario/horario.service';
import { Laboratorio } from '../models/laboratorio/laboratorio';
import { Horario } from '../models/horario/horario';
import { Tutor } from '../models/tutor/tutor';
import { TutorService } from '../service/tutor/tutor.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GestionService } from '../service/gestion/gestion.service';
import { Gestion } from '../models/gestion/gestion';
import { DocenteService } from '../service/docente/docente.service';
import { MateriaService } from '../service/materia/materia.service';
import { Docente } from '../models/docente/docente';
import { Materia } from '../models/materia/materia';
import { GrupoService } from '../service/grupo/grupo.service';

@Component({
  selector: 'app-crearcurso',
  templateUrl: './crearcurso.component.html',
  styleUrls: ['./crearcurso.component.css']
})
export class CrearcursoComponent implements OnInit 
{
  formulario: FormGroup;
  submitted = false;

  laboratorios: Laboratorio[];
  horarios: Horario[];
  tutores: Tutor[];
  grupos: Grupo[];


  constructor(private router: Router, private formBuilder: FormBuilder,
    private serviceLaboratorio: LaboratorioService,
    private serviceHorario: HorarioService,
    private serviceTutor: TutorService,
    private serviceGrupo: GrupoService,
    private serviceCurso: CursoService
    
    )
    {
      this.formulario = this.formBuilder.group(
      {
        laboratorio:["", [Validators.required]],
        tutor:["", [Validators.required]],
        horario:["", [Validators.required]],
        dia:["",[Validators.required]],
        grupo:["",[Validators.required]]
      })
      this.ngOnInit();
    }
  ngOnInit() 
  {
    this.serviceLaboratorio.listar().subscribe(data => (this.laboratorios =data));
    this.serviceHorario.listar().subscribe(data => (this.horarios= data));
    this.serviceTutor.listar().subscribe(data => (this.tutores= data));
    this.serviceGrupo.listar().subscribe(data=>(this.grupos= data));
  
  }
 
  get f() { return this.formulario.controls}

  crearCurso()
  {
    this .submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de guardar este Curso?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log(this.formulario.value);
        this.serviceCurso.registrar(this.formulario.value).subscribe
        (
          (curso: Curso)=>
          {
            console.log(curso);
            Swal.fire(
              'Registro de Curso exitoso!',
              'Muy Bien',
              'success'
            )
          },
          (error) =>
          {
            console.log("Error...!!!", error);
          }
        )

        this.router.navigate(['/inicioadministrador']);
      }
    })
  }
}
