import { Component, OnInit } from '@angular/core';
import { Materia } from '../models/materia/materia';
import { CursoService } from '../service/curso/curso.service';
import { Curso } from '../models/curso/curso';
import { Docente } from '../models/docente/docente';
import { Horario } from '../models/horario/horario';
import { Tutor } from '../models/tutor/tutor';
import { Laboratorio } from '../models/laboratorio/laboratorio';
import { GrupoService } from '../service/grupo/grupo.service';
import { Inscripcion } from '../models/inscripcion/inscripcion';
import { Estudiante } from '../models/estudiante/estudiante';
import { Router } from '@angular/router';
import { InscripcionService } from '../service/inscripcion/inscripcion.service';
import { EstudianteService } from '../service/estudiante/estudiante.service';

@Component({
  selector: 'app-inscripcionestudiante',
  templateUrl: './inscripcionestudiante.component.html',
  styleUrls: ['./inscripcionestudiante.component.css']
})
export class InscripcionestudianteComponent implements OnInit 
{
  cursos: Curso[];
  curso: Curso;
  docente: Docente;
  materia: Materia;
  horario: Horario;
  laboratorio: Laboratorio;
  tutor: Tutor;

  inscripcion: Inscripcion;
  estudiante: Estudiante = JSON.parse(localStorage.getItem("estudianteLogeado"));

  constructor(private router: Router, private cursoService: CursoService, private grupoService: GrupoService, 
    private inscripcionService: InscripcionService, private estudianteService: EstudianteService) { }

  hideUpdate:boolean = true;

  inscripcion1(): void
  {
    this.hideUpdate = false;
  }
  ngOnInit() 
  {
    this.cursoService.listar().subscribe(data => (this.cursos =data));
  }
  inscribir(id: number)
  {
    const Swal = require('sweetalert2')
    Swal.fire({
    title: 'Esta seguro de Registrarse este Curso?',
    text: "porque se guardaran estos datos!",
    type: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Aceptar',
    cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        console.log(id);
        this.cursoService.buscarId(id).subscribe(data => {
        this.curso = data;
        this.inscripcion =  new Inscripcion();
        this.inscripcion.estudiante = this.estudiante;
        this.inscripcion.curso = this.curso;

        this.inscripcionService.registrar(this.inscripcion).subscribe(dataIns =>{
        console.log(dataIns);
        console.log("Se Inscribio Correctamente");
        })
        })
        Swal.fire(
        'Registro de Curso exitoso!',
        'Muy Bien',
        'success'
        )
      }
      this.router.navigate(['/inicioestudiante']);
    });
  }
}
