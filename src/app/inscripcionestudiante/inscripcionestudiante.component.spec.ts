import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscripcionestudianteComponent } from './inscripcionestudiante.component';

describe('InscripcionestudianteComponent', () => {
  let component: InscripcionestudianteComponent;
  let fixture: ComponentFixture<InscripcionestudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscripcionestudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscripcionestudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
