import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstudianteService } from "../service/estudiante/estudiante.service";
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { Estudiante } from '../models/estudiante/estudiante';

@Component({
  selector: 'app-loginestudiante',
  templateUrl: './loginestudiante.component.html',
  styleUrls: ['./loginestudiante.component.css']
})

export class LoginestudianteComponent implements OnInit 
{
  estudiante: Estudiante;
  formulariologin: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, 
    private router: Router,
    private estudianteService: EstudianteService)
  {
    this.formulariologin = this.formBuilder.group({
      codSis: ['',Validators.required],
      contrasenia: ['',Validators.required],
    }
    )
  }
  ngOnInit() 
  {
  }
  get f() { return this.formulariologin.controls; }
  
  onSubmit()
  {
    this.submitted = true;

    if (this.formulariologin.valid) 
    {
      const sis = this.formulariologin.get('codSis').value;
      const contrasenia = this.formulariologin.get('contrasenia').value;

      this.estudianteService.validar(sis, contrasenia).subscribe(
        respuesta => {
          if (this.formulariologin.invalid) {
            return;
          }
          localStorage.setItem("estudianteLogeado", JSON.stringify(respuesta));
          const Swal = require('sweetalert2')
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'success',
            title: 'Conectado Correctamente'
          })
          this.router.navigate(['inicioestudiante']);
        },
        (error) => {
          if (error.status === 404) {
            console.log('no encontrado');
            const Swal = require('sweetalert2')
            Swal.fire({
              type: 'error',
              title: 'Upss...',
              text: '¡Algo salió mal! Verifica que tus datos que sean correctos ',
            })
          } else {
            console.log('error inesperado');
          }
        }
      );
    }
  }
}
