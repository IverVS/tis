import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { InicioComponent } from './inicio/inicio.component';
import { LoginestudianteComponent } from './loginestudiante/loginestudiante.component';
import { RegistroestudianteComponent } from './registroestudiante/registroestudiante.component';
import { InicioestudianteComponent } from './inicioestudiante/inicioestudiante.component';
import { InscripcionestudianteComponent } from './inscripcionestudiante/inscripcionestudiante.component';
import { InscribirestudianteComponent } from './inscribirestudiante/inscribirestudiante.component';
import { RegistroauxiliarComponent } from './registroauxiliar/registroauxiliar.component';
import { LoginauxiliarComponent } from './loginauxiliar/loginauxiliar.component';
import { InicioauxiliarComponent } from './inicioauxiliar/inicioauxiliar.component';
import { CrearactividadComponent } from './crearactividad/crearactividad.component';

import { HttpClientModule} from '@angular/common/http';
import { RegistrodocenteComponent } from './registrodocente/registrodocente.component';
import { LogindocenteComponent } from './logindocente/logindocente.component';
import { IniciodocenteComponent } from './iniciodocente/iniciodocente.component';
import { HomeestudianteComponent } from './homeestudiante/homeestudiante.component';
import { HomeauxiliarComponent } from './homeauxiliar/homeauxiliar.component';
import { HomedocenteComponent } from './homedocente/homedocente.component';
import { VerficardocenteComponent } from './verficardocente/verficardocente.component';
import { VerficarauxiliarComponent } from './verficarauxiliar/verficarauxiliar.component';
import { HorariodocenteComponent } from './horariodocente/horariodocente.component';
import { HorariodocentesComponent } from './horariodocentes/horariodocentes.component';
import { LoginadministradorComponent } from './loginadministrador/loginadministrador.component';
import { InicioadministradorComponent } from './inicioadministrador/inicioadministrador.component';
import { CrearhorarioComponent } from './crearhorario/crearhorario.component';
import { SeleccionmateriaComponent } from './seleccionmateria/seleccionmateria.component';
import { CrearmateriaComponent } from './crearmateria/crearmateria.component';
import { ListahorariosComponent } from './listahorarios/listahorarios.component';
import { BuscarestudianteComponent } from './buscarestudiante/buscarestudiante.component';
import { CrearhoralaboratorioComponent } from './crearhoralaboratorio/crearhoralaboratorio.component';
import { CrearcursoComponent } from './crearcurso/crearcurso.component';
import { CreargestionComponent } from './creargestion/creargestion.component';
import { FilterPipe } from './pipes/filter.pipe';
import { CreargrupoComponent } from './creargrupo/creargrupo.component';
import { CrearnuevaactividadComponent } from './crearnuevaactividad/crearnuevaactividad.component';
import { SelecciongrupoComponent } from './selecciongrupo/selecciongrupo.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';

const routes: Routes = [
  
  { path : 'loginestudiante', component: LoginestudianteComponent},
  { path : 'registroestudiante', component: RegistroestudianteComponent},
  { path : 'inicio', component: InicioComponent},
  { path : 'inicioestudiante', component: InicioestudianteComponent},
  { path : 'inscripcionestudiante', component: InscripcionestudianteComponent},
  { path : 'inscribirestudiante', component: InscribirestudianteComponent},
  { path : 'homeestudiante', component: HomeestudianteComponent},

  { path : 'registroauxiliar', component: RegistroauxiliarComponent},
  { path : 'loginauxiliar', component: LoginauxiliarComponent},
  { path : 'inicioauxiliar', component: InicioauxiliarComponent},
  { path : 'crearactividad', component: CrearactividadComponent},
  { path : 'homeauxiliar', component: HomeauxiliarComponent},
  { path : 'verificarauxiliar', component: VerficarauxiliarComponent},
  { path : 'crearnuevaactividad', component: CrearnuevaactividadComponent},
  { path : 'selecciongrupo', component: SelecciongrupoComponent},

  { path : 'registrodocente', component: RegistrodocenteComponent},
  { path : 'logindocente', component: LogindocenteComponent},
  { path : 'iniciodocente', component: IniciodocenteComponent},
  { path : 'homedocente', component: HomedocenteComponent},
  { path : 'verificardocente', component: VerficardocenteComponent},

  { path : 'horariodocente', component: HorariodocenteComponent},
  { path : 'horariodocentes', component: HorariodocentesComponent},
  
  { path : 'loginadministrador', component: LoginadministradorComponent},
  { path : 'inicioadministrador', component: InicioadministradorComponent},
  { path : 'crearcurso', component: CrearcursoComponent},
  { path : 'crearhorario', component: CrearhorarioComponent},
  { path : 'seleccionmateria', component: SeleccionmateriaComponent},
  { path : 'crearmateria', component: CrearmateriaComponent},
  { path : 'listahorarios', component: ListahorariosComponent},
  { path : 'buscarestudiante', component: BuscarestudianteComponent},
  { path : 'crearhoralaboratorio', component: CrearhoralaboratorioComponent},
  { path : 'creargestion', component: CreargestionComponent},
  { path : 'creargrupo', component: CreargrupoComponent},
  { path : 'asistencia', component: AsistenciaComponent},


  
];

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    InicioComponent,
    LoginestudianteComponent,
    RegistroestudianteComponent,
    InicioestudianteComponent,
    InscripcionestudianteComponent,
    InscribirestudianteComponent,

    RegistroauxiliarComponent,
    LoginauxiliarComponent,
    InicioauxiliarComponent,
    CrearactividadComponent,
    
    RegistrodocenteComponent,
    LogindocenteComponent,
    IniciodocenteComponent,
    HomeestudianteComponent,
    HomeauxiliarComponent,
    HomedocenteComponent,
    VerficardocenteComponent,
    VerficarauxiliarComponent,
    
    HorariodocenteComponent,
    HorariodocentesComponent,
    LoginadministradorComponent,
    InicioadministradorComponent,
    CrearhorarioComponent,
    SeleccionmateriaComponent,
    CrearmateriaComponent,
    ListahorariosComponent,
    BuscarestudianteComponent,
    CrearhoralaboratorioComponent,
    CrearcursoComponent,
    CreargestionComponent,
    FilterPipe,
    CreargrupoComponent,
    CrearnuevaactividadComponent,
    SelecciongrupoComponent,
    AsistenciaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
