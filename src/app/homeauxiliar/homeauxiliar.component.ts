import { Component, OnInit } from '@angular/core';
import { Tutor } from '../models/tutor/tutor';

@Component({
  selector: 'app-homeauxiliar',
  templateUrl: './homeauxiliar.component.html',
  styleUrls: ['./homeauxiliar.component.css']
})
export class HomeauxiliarComponent implements OnInit {

  tutor: Tutor = JSON.parse(localStorage.getItem("tutorRegistrado")); 
  constructor() { }

  ngOnInit() {
  }

}
