import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Curso } from './../models/curso/curso';
import { Component, OnInit } from '@angular/core';
import { CursoService } from '../service/curso/curso.service';

@Component({
  selector: 'app-listahorarios',
  templateUrl: './listahorarios.component.html',
  styleUrls: ['./listahorarios.component.css']
})
export class ListahorariosComponent implements OnInit {

  cursos: Curso[];
  constructor(private router: Router, private cursoService: CursoService) { }

  ngOnInit() {
    this.cursoService.listar().subscribe(data => (this.cursos =data));
  }

}
