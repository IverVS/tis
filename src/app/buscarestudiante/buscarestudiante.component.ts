import { Component, OnInit } from '@angular/core';
import { EstudianteService } from '../service/estudiante/estudiante.service';
import { Estudiante } from '../models/estudiante/estudiante';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buscarestudiante',
  templateUrl: './buscarestudiante.component.html',
  styleUrls: ['./buscarestudiante.component.css']
})
export class BuscarestudianteComponent implements OnInit 
{
  formulario: FormGroup;
  estudiantes: Estudiante[];
  estudent: Estudiante;
  constructor(private router: Router, private formBuilder: FormBuilder, private service: EstudianteService) 
  {
  }
  filterPost = '';
  ngOnInit() 
  {
    this.service.listar().subscribe(data => (this.estudiantes = data));
    //console.log(this.estudiantes);
    //this.estudiantes.push(this.estudent);
  }

  buscar(estudiante: Estudiante)
  {
    //this.service.estudianteId(estudiante).subscribe(data =>{
      //this.estudiantes = this.estudiantes.filter(e => e!== id);
    localStorage.setItem("id",estudiante.idEstudiante.toString());
    this.router.navigate(["asistencia"]);
  }
}
