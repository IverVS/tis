import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarestudianteComponent } from './buscarestudiante.component';

describe('BuscarestudianteComponent', () => {
  let component: BuscarestudianteComponent;
  let fixture: ComponentFixture<BuscarestudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarestudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarestudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
