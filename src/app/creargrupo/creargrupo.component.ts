import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DocenteService } from '../service/docente/docente.service';
import { GestionService } from '../service/gestion/gestion.service';
import { MateriaService } from '../service/materia/materia.service';
import { Docente } from '../models/docente/docente';
import { Gestion } from '../models/gestion/gestion';
import { Materia } from '../models/materia/materia';
import { Grupo } from '../models/grupo/grupo';
import { GrupoService } from '../service/grupo/grupo.service';

@Component({
  selector: 'app-creargrupo',
  templateUrl: './creargrupo.component.html',
  styleUrls: ['./creargrupo.component.css']
})
export class CreargrupoComponent implements OnInit 
{

  formulario: FormGroup;
  submitted = false;

  docentes: Docente[];
  gestiones: Gestion[];
  materias: Materia[];

  constructor(private router: Router, private formBuilder:FormBuilder, 
    private serviceDocente: DocenteService,
    private serviceGestion: GestionService,
    private serviceMateria: MateriaService,
    private serviceGrupo: GrupoService
    )
  { 
    this.formulario = this.formBuilder.group({
      gestion: ["", [Validators.required]],
      docente: ["", [Validators.required]],
      materia: ["", [Validators.required]]
    });
  }

  ngOnInit() 
  {
    this.serviceDocente.listar().subscribe(data => (this.docentes =data));
    this.serviceGestion.listar().subscribe(data => (this.gestiones= data));
    this.serviceMateria.listar().subscribe(data => (this.materias= data));
  }
  get f() { return this.formulario.controls}
 
  crearGrupo()
  {
    this .submitted = true;

    if(this.formulario.invalid)
    {
      return;
    }
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de guardar este Grupo?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        console.log("formulario funcionando", this.formulario.value) ;
        this.serviceGrupo.registrar(this.formulario.value).subscribe
        (
            //var a = this.formulario.ge
          
          (grupo: Grupo) =>
          {
            console.log(grupo);
            Swal.fire(
              'Registro de horario exitoso!',
              'Muy Bien',
              'success'
            )
            this.router.navigate(['/inicioadministrador']);
          },
          (error) => 
          {
            console.log("Error!!!", error);
          }
        )

        //this.router.navigate(['/inicioadministrador'])
      }
    })
  }
}
