import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorariodocentesComponent } from './horariodocentes.component';

describe('HorariodocentesComponent', () => {
  let component: HorariodocentesComponent;
  let fixture: ComponentFixture<HorariodocentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorariodocentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorariodocentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
