import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-horariodocentes',
  templateUrl: './horariodocentes.component.html',
  styleUrls: ['./horariodocentes.component.css']
})
export class HorariodocentesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  inscribirest()
  {
    const Swal = require('sweetalert2')
    Swal.fire({
      title: 'Esta seguro de inscribirse en ese horario que seleccionó?',
      text: "porque se guardaran estos datos!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //aqui se ara el paso de guardar el registro en la bd
        Swal.fire(
          'Registro de horario exitoso!',
          'Muy Bien',
          'success'
        )
        this.router.navigate(['/inscripcionestudiante'])
      }
    })
  }
}
