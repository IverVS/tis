import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecciongrupoComponent } from './selecciongrupo.component';

describe('SelecciongrupoComponent', () => {
  let component: SelecciongrupoComponent;
  let fixture: ComponentFixture<SelecciongrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecciongrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecciongrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
