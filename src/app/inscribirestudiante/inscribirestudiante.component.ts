import { Component, OnInit } from '@angular/core';
import { DocenteService } from '../service/docente/docente.service';
import { Docente } from '../models/docente/docente';
import { CursoService } from '../service/curso/curso.service';
import { Curso } from '../models/curso/curso';

@Component({
  selector: 'app-inscribirestudiante',
  templateUrl: './inscribirestudiante.component.html',
  styleUrls: ['./inscribirestudiante.component.css']
})
export class InscribirestudianteComponent implements OnInit 
{
  cursos: Curso[];
  constructor(private serviceCurso: CursoService) 
  {
  }

  ngOnInit() 
  {
    this.serviceCurso.listar().subscribe(data => (this.cursos =data));
  }

}
