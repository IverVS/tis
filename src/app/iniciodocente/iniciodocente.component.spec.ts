import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciodocenteComponent } from './iniciodocente.component';

describe('IniciodocenteComponent', () => {
  let component: IniciodocenteComponent;
  let fixture: ComponentFixture<IniciodocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciodocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciodocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
