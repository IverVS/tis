import { Component, OnInit } from '@angular/core';
import { Docente } from '../models/docente/docente';
import { Router } from '@angular/router';

@Component({
  selector: 'app-iniciodocente',
  templateUrl: './iniciodocente.component.html',
  styleUrls: ['./iniciodocente.component.css']
})
export class IniciodocenteComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  docente: Docente= JSON.parse(localStorage.getItem("docenteLogeado"));


  hideUpdate:boolean = true;
  hidenUpdate:boolean = true;
  
  editEmpleado(): void
  {
    this.hideUpdate = false;
  }
  inscripcion(): void
  {
    this.hidenUpdate = false;
  }

  cerrarSesion(){
    this.router.navigate(['inicio']);
  }

}
