import { Component, OnInit } from '@angular/core';
import { EstudianteService } from '../service/estudiante/estudiante.service';
import { Estudiante } from '../models/estudiante/estudiante';
import { Router } from '@angular/router';
import { AsistenciaService } from '../service/asistencia/asistencia.service';
import { Asistencia } from '../models/asistencia/asistencia';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})
export class AsistenciaComponent implements OnInit 
{
  date: Date = new Date();
  settings = 
  {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MM-yyyy hh:mm a',
    defaultOpen: false,
    closeOnSelect: false
  };

  estudiante: Estudiante = new Estudiante();
  asistencia: Asistencia;
  fecha: string;
  constructor(private router: Router, private service: EstudianteService, 
    private serviceAsistencia: AsistenciaService) { }

  ngOnInit() 
  {
    this.listarEstudianteID();
    console.log(this.estudiante.idEstudiante);
  }
  listarEstudianteID()
  {
    let id = localStorage.getItem("id");
    this.service.estudianteId(+id)
    .subscribe(data =>{
      this.estudiante = data;
    })
  }
  registrarAsistencia()
  {
    const Swal = require('sweetalert2')
    Swal.fire({
    title: 'Esta seguro de Tomar Asistencia?',
    text: "porque se guardaran estos datos!",
    type: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Aceptar',
    cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) 
      {
        //console.log(this.estudiante);
        //console.log(new Date(Date.now()));  //"xxxx-mm-dd"
        let d = new Date();
        this.fecha = d.getFullYear()+"-"+(d.getUTCMonth()+1)+"-"+(d.getUTCDate());
        console.log(this.fecha);

        this.asistencia = new Asistencia();
        this.asistencia.estudiante = this.estudiante;
        this.asistencia.fecha = this.fecha;
        this.serviceAsistencia.registrar(this.asistencia).subscribe(data =>
        {
          console.log(data.estudiante);
        });
        Swal.fire(
        'Registro de Asistencia exitoso!',
        'Muy Bien',
        'success'
        )
      }
    });
  }
}
