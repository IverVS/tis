import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomedocenteComponent } from './homedocente.component';

describe('HomedocenteComponent', () => {
  let component: HomedocenteComponent;
  let fixture: ComponentFixture<HomedocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomedocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomedocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
