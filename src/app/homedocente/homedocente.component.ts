import { Component, OnInit } from '@angular/core';
import { Docente } from '../models/docente/docente';

@Component({
  selector: 'app-homedocente',
  templateUrl: './homedocente.component.html',
  styleUrls: ['./homedocente.component.css']
})
export class HomedocenteComponent implements OnInit {

  docente: Docente= JSON.parse(localStorage.getItem("docenteRegistrado"));

  constructor() { }
  
  ngOnInit() {
  }

}
