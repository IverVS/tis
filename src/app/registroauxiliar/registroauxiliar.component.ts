import { Component, OnInit } from '@angular/core';
import {TutorService} from "../service/tutor/tutor.service";
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { Tutor} from '../models/tutor/tutor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registroauxiliar',
  templateUrl: './registroauxiliar.component.html',
  styleUrls: ['./registroauxiliar.component.css']
})
export class RegistroauxiliarComponent implements OnInit {

  formularioTutor: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private tutorService: TutorService)
  {
    this.formularioTutor = this.formBuilder.group(
      {
        nombre: ['',Validators.required],
        apPaterno:['',Validators.required],
        apMaterno:['',Validators.required],
        cedula:['',Validators.required],
        celular:['',Validators.required],
        codSis:['',Validators.required],
        contrasenia:['',Validators.required],
        email:['',Validators.email],
      }
    )
  }

  ngOnInit() {
  }

  get f() { return this.formularioTutor.controls; }

  crearTutor()
  {
    this.submitted = true;

    if (this.formularioTutor.invalid) 
    {
      return;
    }
      const Swal = require('sweetalert2')
      Swal.fire({
      title: 'Estas Seguro de Guardar?',
      text: "porque se guardaran estos datos",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      
      cancelButtonText: 'Cancelar'
      }).then((result) => 
      {
        if (result.value) 
        {
          console.log("formulario funcionando", this.formularioTutor.value) ;
          this.tutorService.guardarTutor(this.formularioTutor.value).subscribe
          (
            (tutor: Tutor) => 
            {
                if(tutor == null){
                alert("Estudiante ya existe");
                 window.location.reload();
                }
                else{
                  console.log(tutor);
                  localStorage.setItem("tutorRegistrado", JSON.stringify(tutor));
                  Swal.fire(
                    'Bienvenido!  ',
                    'Ahora puedes iniciar tu Sesión',
                    'success')

                    this.router.navigate(['homeauxiliar']);
                   
                   
                  
                }

              
              
            },
            (error) => 
            {
              console.log("Error!!!", error);
            }
          )
          
          
          
          
        }
      })
  }

}
