import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroauxiliarComponent } from './registroauxiliar.component';

describe('RegistroauxiliarComponent', () => {
  let component: RegistroauxiliarComponent;
  let fixture: ComponentFixture<RegistroauxiliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroauxiliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroauxiliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
