import { Component, OnInit } from '@angular/core';
import { Tutor } from '../models/tutor/tutor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicioauxiliar',
  templateUrl: './inicioauxiliar.component.html',
  styleUrls: ['./inicioauxiliar.component.css']
})
export class InicioauxiliarComponent implements OnInit {

      tutor: Tutor = JSON.parse(localStorage.getItem("tutorLogeado")); 

  constructor(private router: Router) { }


  hideUpdate:boolean = true;
  hidenUpdate:boolean = true;
  
  editEmpleado(): void
  {
    this.hideUpdate = false;
  }
  inscripcion(): void
  {
    this.hidenUpdate = false;
  }
  ngOnInit() {
  }

  cerrarSesion(){
    this.router.navigate(['inicio']);
  }


}
