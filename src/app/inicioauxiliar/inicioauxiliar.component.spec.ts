import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioauxiliarComponent } from './inicioauxiliar.component';

describe('InicioauxiliarComponent', () => {
  let component: InicioauxiliarComponent;
  let fixture: ComponentFixture<InicioauxiliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioauxiliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioauxiliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
